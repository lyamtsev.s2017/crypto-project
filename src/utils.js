import Bowser from "bowser";

export default function isMobilePlatform() {
  return Bowser.getParser(window.navigator.userAgent).getPlatformType() === 'mobile'
}
