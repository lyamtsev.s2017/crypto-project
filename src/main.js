import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import store from './store'
import router from './router'
import AOS from 'aos'
import 'aos/dist/aos.css'
import VueAnime from 'vue-animejs';
import VueWaypoint from "vue-waypoint/src";

Vue.config.productionTip = false
Vue.use(VueAnime)
Vue.use(VueWaypoint)

new Vue({
  store,
  router,
  render: h => h(App),
  mounted() {
    AOS.init()
  }
}).$mount('#app')
